import React from 'react';
import Button from "../Button/Button";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const Form = (props) => {
    return (
        <form className="mt-4 pt-4" onSubmit={props.onSubmit}>
            <div className="mt-4 pt-4">
                <DropdownButton
                    alignRight
                    title="Category"
                    id="dropdown-menu-align-right"
                    onSelect={props.handleSelect}
                >
                    <Dropdown.Item eventKey="about">About</Dropdown.Item>
                    <Dropdown.Item eventKey="articles">Articles</Dropdown.Item>
                    <Dropdown.Item eventKey="news">News</Dropdown.Item>
                    <Dropdown.Item eventKey="messages">Messages</Dropdown.Item>
                    <Dropdown.Item eventKey="titles">Titles</Dropdown.Item>
                    <Dropdown.Item eventKey="services">Services</Dropdown.Item>
                    <Dropdown.Item eventKey="contacts">Contacts</Dropdown.Item>
                    <Dropdown.Divider />
                </DropdownButton>
                <span className="input-group-text" id="inputGroup-sizing-default">{props.select}</span>
                <div className="input-group mb-3 mt-4 pt-4">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Title</span>
                    </div>
                    <input type="text" name='title' className="form-control" onChange={props.change}
                           aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-default" value={props.title}/>
                </div>
                <div className="input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Description</span>
                    </div>

                    <textarea className="form-control" name='content' onChange={props.change} aria-label="With textarea" value={props.content}/>
                </div>
                <Button name='Submit'/>
            </div>
        </form>
    );
};

export default Form;