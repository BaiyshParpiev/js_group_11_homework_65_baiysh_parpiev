import React, {useEffect, useState} from 'react';
import {axiosApi} from "../../axiosApi";
import Form from "../Form/Form";

const Change = ({history}) => {
    const [select, setSelect] = useState();
    const [content, setContent] = useState({
        title: '',
        content: ''
    });

    const change = e => {
        const {name, value} = e.target;
        setContent(prev => ({
                ...prev,
                [name]: value,
            }));
    }

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/pages/' + select + '.json');
            setContent(response.data);
        }

        fetchData().catch(e => console.log(e));
    }, [select]);

    const handleSelect=(e)=>{
        setSelect(e)
    }

    const onSubmit = async e => {
        e.preventDefault();
        try {
            await axiosApi.put('/pages/' + select + '.json', {
                ...content,
            });

        } finally {
            history.replace('/pages/' + select);
        }
    }
    return (
        content ? <Form change={e => change(e)} onSubmit={onSubmit} title={content.title} content={content.content} handleSelect={handleSelect}/> : <Form change={e => change(e)} onSubmit={onSubmit} title= '' content= '' handleSelect={handleSelect}/>
    );

};

export default Change;