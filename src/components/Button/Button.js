import React from 'react';

const Button = ({name, click}) => (
    <button onClick={click} className="btn btn-primary mt-4 ml-4">{name}</button>
);

export default Button;