import React from 'react';
import './Layout.css';
import {NavLink} from "react-router-dom";

const Layout = () => {

    return (
        <header className="Toolbar">
            <div className="Toolbar-Logo">logo</div>
            <nav>
                <NavLink to='/' exact>Home</NavLink>
                <NavLink to='/pages/about'>About</NavLink>
                <NavLink to='/pages/contacts'>Contacts</NavLink>
                <NavLink to='/pages/services'>Services</NavLink>
                <NavLink to='/pages/messages'>Messages</NavLink>
                <NavLink to='/pages/articles'>Articles</NavLink>
                <NavLink to='/pages/titles'>Titles</NavLink>
                <NavLink to='/pages/news'>News</NavLink>
                <NavLink to='/pages/admin'>Admin</NavLink>
            </nav>
        </header>
    );
};

export default Layout;