import axios from 'axios';

export const axiosApi = axios.create({
    baseURL : 'https://baiysh-parpiev-default-rtdb.firebaseio.com/'
})