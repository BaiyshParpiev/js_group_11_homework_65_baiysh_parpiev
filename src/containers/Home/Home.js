import React, {useEffect, useState} from 'react';
import {axiosApi} from "../../axiosApi";
import Spinner from "../../components/Spinner/Spinner";

const Home = ({match}) => {
    const [content, setContent] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            if(match.path === '/'){
                const response = await axiosApi.get(  '/pages.json');
                const fetchedOrders = Object.keys(response.data).map(id => {
                    const order = response.data[id];
                    return {...order};
                });
                setContent(fetchedOrders)
            }else{
                const response = await axiosApi.get(match.path + '.json');
                setContent([response.data])
            }
        }

        fetchData().finally(() => setLoading(false))
    }, [match.path])
    return (
        loading ?  <Spinner/> : <div className="container mt-4 pt-4">
            {content.map(c => (
                <div key={c.title} className='mt-4'>
                    <h2>{c.title}</h2>
                    <p>{c.content}</p>
                </div>
            ))}
        </div>
    );
};

export default Home;